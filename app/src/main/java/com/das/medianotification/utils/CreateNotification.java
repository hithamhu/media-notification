package com.das.medianotification.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.media.session.MediaSessionCompat;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.das.medianotification.R;
import com.das.medianotification.model.Track;
import com.das.medianotification.service.NotivicationActionService;

public class CreateNotification {

    public static final String CHANNEL_ID="media Player";
    public static final String ACTION_PLAY="action_play";
    public static final String ACTION_PREVIOES="action_previous";
    public static final String ACTION_NEXT="action_next";

    public static Notification notification;

    public static void createNotification(Context context, Track track,int playButton,int pos,int size) {

        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.O){
            NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(context);
            MediaSessionCompat mediaSessionCompat=new MediaSessionCompat(context,"tag");
            Bitmap icon= BitmapFactory.decodeResource(context.getResources(),track.getImage());

            //button in notification

            PendingIntent intentPeddingPrevious;
            int div_previous;
            if (pos==0){
                intentPeddingPrevious=null;
                div_previous=0;
            }else{
                buttonActionPending(ACTION_PREVIOES,context);
                div_previous=R.drawable.ic_next_previous;

            }
            buttonActionPending(ACTION_PLAY,context);

            ///next
            PendingIntent intentPeddingNext;
            int div_Next;
            if (pos==size){
                intentPeddingNext=null;
                div_Next=0;
            }else{
                buttonActionPending(ACTION_NEXT,context);
                div_Next=R.drawable.ic_skip_next;

            }
            //create notification
            notification= new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_music)
                    .setContentTitle(track.getTitle())
                    .setContentText(track.getArtist())
                    .addAction(div_previous,"previous",buttonActionPending(ACTION_PREVIOES,context))
                    .addAction(playButton,"play",buttonActionPending(ACTION_PLAY,context))
                    .addAction(div_Next,"next",buttonActionPending(ACTION_NEXT,context))
                    .setLargeIcon(icon)
                    .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                            .setShowActionsInCompactView(0,1,2)
                    .setMediaSession(mediaSessionCompat.getSessionToken()))

                    .setOnlyAlertOnce(true)
                    .setShowWhen(false)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .build();

            notificationManagerCompat.notify(1,notification);

        }
    }



    private static PendingIntent buttonActionPending(String actionName, Context context){
        Intent intent=new Intent(context, NotivicationActionService.class)
                .setAction(actionName);
       PendingIntent pendingIntent=PendingIntent.getBroadcast(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

       return pendingIntent;

    }
}
