package com.das.medianotification;

import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.das.medianotification.callback.Playback;
import com.das.medianotification.model.Track;
import com.das.medianotification.service.OnClearFromService;
import com.das.medianotification.utils.CreateNotification;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        Playback {
    private static final String TAG = "MainActivity";
    ImageButton imageButton;
    TextView musicInfo;

    NotificationManager notificationManager;
    List<Track> trackList;

    boolean isPlaying=false;
    int position=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        populatMusic();
        
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            createChannel();
            registerReceiver(broadcastReceiver, new IntentFilter("Track"));
            startService(new Intent(getBaseContext(), OnClearFromService.class));
        }
    }

    private void createChannel() {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=new NotificationChannel(CreateNotification.CHANNEL_ID,
                    "MusicPlayer",NotificationManager.IMPORTANCE_LOW);
            notificationManager=getSystemService(NotificationManager.class);
            if (notificationManager !=null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    private void init(){
        imageButton=findViewById(R.id.play_Btn);
        musicInfo=findViewById(R.id.musicTitle_txt);

        imageButton.setOnClickListener(this);
    }

    private void populatMusic(){
        trackList=new ArrayList<>();
        trackList.add(new Track("Track 1","Artics 1",R.drawable.a_1));
        trackList.add(new Track("Track 1","Artics 1",R.drawable.a_2));
        trackList.add(new Track("Track 1","Artics 1",R.drawable.a_2));
        trackList.add(new Track("Track 1","Artics 1",R.drawable.a_1));
    }


    @Override
    public void onClick(View v) {
        if (v.getId()==imageButton.getId()){
            if (isPlaying){
                onTrackPause();
            }else {
                onTrackPlay();
            }
        }
    }

    BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getExtras().getString("actionIntent");
            switch (action){
                case CreateNotification.ACTION_PREVIOES:
                    onTrackPrevious();
                    break;
                case CreateNotification.ACTION_PLAY:
                    if (isPlaying) {
                        onTrackPause();
                    }else {
                        onTrackPlay();
                    }
                    break;
                case CreateNotification.ACTION_NEXT:
                    onTrackNext();
                    break;

            }
        }
    };

    @Override
    public void onTrackPrevious() {
        if (position>0) {
            position--;
            notificationAtion(position, R.drawable.ic_pause_circle);
        }else {
            notificationAtion(0, R.drawable.ic_pause_circle);
        }
    }

    @Override
    public void onTrackPlay() {

        notificationAtion(position,R.drawable.ic_pause_circle);
        imageButton.setImageResource(R.drawable.ic_pause_circle);
        isPlaying=true;
    }

    @Override
    public void onTrackPause() {
        notificationAtion(position,R.drawable.ic_play_circle);
        imageButton.setImageResource(R.drawable.ic_play_circle);
        isPlaying=false;
    }

    @Override
    public void onTrackNext() {
        if (position<trackList.size()-1) {
            position++;
            notificationAtion(position, R.drawable.ic_pause_circle);
        }else {
            notificationAtion(trackList.size()-1, R.drawable.ic_pause_circle);
        }
    }

    private void notificationAtion(int position,int image){
        CreateNotification.createNotification(MainActivity.this,trackList.get(position)
        ,image,position,trackList.size()-1);
        musicInfo.setText(trackList.get(position).getTitle());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            notificationManager.cancelAll();
        }
        unregisterReceiver(broadcastReceiver);
    }
}
