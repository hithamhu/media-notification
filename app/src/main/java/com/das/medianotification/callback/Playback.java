package com.das.medianotification.callback;

public interface Playback {

    void onTrackPrevious();
    void onTrackPlay();
    void onTrackPause();
    void onTrackNext();
}
